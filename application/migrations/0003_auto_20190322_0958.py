# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import apps as global_apps
from django.db import migrations


def import_products_app(apps, schema_editor):
    try:
        OldProductCategory = apps.get_model("products", "ProductCategory")
        OldProduct = apps.get_model("products", "Product")
        OldIngredient = apps.get_model("products", "Ingredient")
    except LookupError:
        # The old app isn't installed.
        return

    NewProductCategory = apps.get_model("application", "ProductCategory")
    NewProduct = apps.get_model("application", "Product")
    NewIngredient = apps.get_model("application", "Ingredient")

    for old_product_category in OldProductCategory.objects.all():
        NewProductCategory.objects.create(
            id=old_product_category.id, name=old_product_category.name
        )

    for old_product in OldProduct.objects.all():
        category = NewProductCategory.objects.get(name=old_product.category.name)
        NewProduct.objects.create(
            id=old_product.id,
            name=old_product.name,
            description=old_product.description,
            image=old_product.image,
            category=category,
        )

    for old_ingredient in OldIngredient.objects.all():
        product = NewProduct.objects.get(name=old_ingredient.product.name)
        NewIngredient.objects.create(
            id=old_ingredient.id,
            name=old_ingredient.name,
            origin=old_ingredient.origin,
            url=old_ingredient.url,
            product=product,
        )


class Migration(migrations.Migration):
    operations = [migrations.RunPython(import_products_app, migrations.RunPython.noop)]

    dependencies = [("application", "0002_auto_20190322_0955")]

    if global_apps.is_installed("products"):
        dependencies.append(("products", "0005_auto_20150425_0931"))

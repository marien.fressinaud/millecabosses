# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('application', '0005_auto_20190322_1057'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='ingredient',
            options={'ordering': ['name'], 'verbose_name': 'Ingr\xe9dient'},
        ),
        migrations.AlterModelOptions(
            name='link',
            options={'ordering': ['title'], 'verbose_name': 'Lien'},
        ),
        migrations.AlterModelOptions(
            name='product',
            options={'ordering': ['name'], 'verbose_name': 'Produit'},
        ),
        migrations.AlterModelOptions(
            name='productcategory',
            options={'ordering': ['name'], 'verbose_name': 'Cat\xe9gorie de produits', 'verbose_name_plural': 'Cat\xe9gories de produits'},
        ),
        migrations.AlterModelOptions(
            name='textblock',
            options={'ordering': ['order'], 'verbose_name': 'Bloc de texte'},
        ),
    ]

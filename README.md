# millecabosses

Sources du site [chocolaterie1000cabosses.fr](http://chocolaterie1000cabosses.fr).
Ce site a été développé un peu à la va-vite en 2015 alors que je ne connaissais
pas vraiment Django, merci de votre indulgence quant au code produit <3

## Environnement technique

Ce site tourne sur une base Python 2 et Django. Idéalement, j'aimerais le
passer en Python 3. Encore plus idéalement j'aimerais partir sur un site
statique, mais le monde n'est pas parfait.

## Développement

Assurez-vous d'avoir Python 2 installé avec `pip`. C'est normalement le cas
mais je vous laisse chercher sur Internet pour savoir comment faire dans le cas
contraire.

Vous pouvez utiliser un "virtualenv" si ça vous chante, ce dépôt n'est pas là
pour vous apprendre à mettre en place un projet Python.

Installez ensuite les dépendances :

```console
$ pip install -r requirements.txt --user
```

L'une des dépendances est `Pillow` qui a besoin d'être compilé. En cas de
soucis, je vous laisse jeter un œil à [la documentation officielle](https://pillow.readthedocs.io/en/3.1.x/installation.html).

Vous pouvez désormais lancer le projet avec :

```console
$ python manage.py runserver
```

Le site devrait être accessible à l'adresse [127.0.0.1:8000](http://127.0.0.1:8000).

Vous aurez sans doute besoin d'exécuter les migrations de la base de données
avec :

```console
$ python manage.py migrate
```

## Production

Le site peut être assez facilement mis en production grâce à l'image Docker
fournie. Pour la construire :

```console
$ docker build -t millecabosses .
```

Elle est également mise à disposition sur le [hub Docker](https://hub.docker.com/r/marienfressinaud/millecabosses),
vous pouvez la récupérer en faisant :

```console
$ docker pull marienfressinaud/millecabosses
```

Ensuite, personnellement j'utilise un fichier [`docker-compose`](https://docs.docker.com/compose/).
Il ne sert pas forcément à grand chose, mais je trouve que ça facilite la
lisibilité.

```yaml
version: '3'

services:
  millecabosses:
    image: marienfressinaud/millecabosses:latest
    restart: unless-stopped
    ports:
      - "127.0.0.1:8000:8000"
    volumes:
      - ./data:/app/data:z
      - ./public:/app/public:z
    environment:
      APP_SECRET_KEY: "une chaine de caractères aléatoires"
      APP_HOSTS: "chocolaterie1000cabosses.fr,www.chocolaterie1000cabosses.fr"
```

Le répertoire `data` qui est monté dans le conteneur va contenir la base de
données SQLite tandis que le répertoire `public` contiendra les fichiers
`static` ainsi que les fichiers `media` qui seront téléversés par
l'administrateur du site. Cela permet de faire vivre les données entre deux
démarrages du conteneur Docker et de sauvegarder les données.

Pour lancer le service, il ne reste plus qu'à lancer :

```console
$ docker-compose up
```

Simple. Vous aurez ensuite besoin de collecter les fichiers statiques et
exécuter les migrations :

```console
$ docker-compose run --rm millecabosses python manage.py collectstatic
$ docker-compose run --rm millecabosses python manage.py migrate
```

Enfin, afin d'exposer le site sur Internet, je recommande de passer par un
"reverse proxy" comme Nginx. Le fichier de configuration qui va bien (mais
simplifié) :

```nginx
server {
  listen 80;
  listen [::]:80;

  server_name chocolaterie1000cabosses.fr www.chocolaterie1000cabosses.fr

  root /var/www/millecabosses/public;
  index index.html;

  location / {
    try_files $uri @millecabosses;
  }

  location @millecabosses {
    proxy_pass http://127.0.0.1:8000;
    include proxy_params;
  }
}
```

Il ne reste plus qu'à mettre en place un système automatique de sauvegarde des
données. Les deux répertoires importants à sauver sont, comme expliqué, plus
haut `data` et `public/media` (`public/static` étant généré automatiquement et
exposé uniquement pour que Nginx puisse servir ces fichiers).
